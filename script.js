const searchInput = document.querySelector("#poke-input");
const searchBtn = document.querySelector(".btn-search");
const pokeContainer = document.querySelector(".poke-container");
const wrappenPokemon = document.querySelector("#wrappen-pokemon");
const button = document.querySelector(".btn-link");

const colors = {
    fire: '#FDDFDF',
    grass: '#DEFDE0',
    eletric: '#FCF7DE',
    water: '#DEF3FD',
    ground: '#f4e7da',
    rock: '#d5d5d4',
    fairy: '#fceaff',
    poison: '#d6b3ff',
    bug: '#f8d5a3',
    dragon: '#97b3e6',
    psychic: '#eaeda1',
    flying: '#F5F5F5',
    fighting: '#E6E0D4',
    normal: '#F5F5F5',
    ice: '#e0f5ff',
}
const pokeCount = 151;

const initPokemon = async() => {
    for (let i = 1; i <= pokeCount; i++) {
        await getPokemon(i);
    }
}

const getPokemon = async(id) => {
    let url = `https://pokeapi.co/api/v2/pokemon/${id}`;
    let res = await fetch(url);
    let data = await res.json();
    createPokemonBox(data);
}

const createPokemonBox = (pokemon) => {
    const name = pokemon.name[0].toUpperCase() + pokemon.name.slice(1);
    const id = pokemon.id.toString().padStart(3, '0');
    const weight = pokemon.weight;
    const type = pokemon.types[0].type.name;
    const color = colors[type];
    const pokemonEl = document.createElement("div");
    pokemonEl.classList.add("col-12", "col-md-6", "col-lg-4", "pb-5", "poke-box", "text-center", "mb-4", "mx-auto", "ms-lg-5");
    pokemonEl.style.backgroundColor = `${color}`;
    pokemonEl.innerHTML = `
        <img src="https://assets.pokemon.com/assets/cms2/img/pokedex/full/${id}.png" alt="${name}" class="img-fluid">
        <h4 class="poke-name fw-bold">${name}</h4>
        <p class="poke-id mb-0">#${id}</p>
        <p class="poke-weight mb-0">${weight} Kg</p>
        <p class="poke-type">Type: ${type}</p>
    `

    wrappenPokemon.appendChild(pokemonEl);
}

searchInput.addEventListener("input", (e) => {
    const pokeNames = document.querySelectorAll('.poke-name')
    const search = searchInput.value.toLowerCase();
    const pokemons = document.querySelectorAll(".poke.box");

    pokeNames.forEach((pokeName) => {
        pokeName.parentElement.style.display = 'block';

        if (!pokeName.innerHTML.toLowerCase().includes(search)) {

            pokeName.parentElement.style.display = 'none';
        }
    })
})

initPokemon();

window.addEventListener('scroll', () => {
    if (window.scrollY > 100) {
        button.classList.add('scrolled')
    } else {
        button.classList.remove('scrolled')
    }
})